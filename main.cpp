#include <QCoreApplication>
#include <iostream>
#include "src/imgur.h"

void uploadProgress(double perc);
void imageUrl(QString url);
void deleteHash(QString url);
void uploadError(QNetworkReply::NetworkError err);
void sayGoodbye();

int main(int argc, char *argv[]) {

    // Some options
    bool debug = false;
    bool authAccount = false;
    bool forgetAccount = false;
    bool current = false;
    bool forgetAndAuthAccount = false;
    bool anonymousUpload = false;
    QString uploadFile = "";
    QString delHash = "";
    QString localAccountData = QDir::homePath() + "/.config/ImgurQt/user";

    // If no option/filename specified, or if help requested, display help message
    if(argc == 1 || (argc == 2 && (std::string(argv[1]) == "-h" || std::string(argv[1]) == "--help"))) {
        std::cout << R"d(
 Qt-based imgur.com uploader, using imgur.com API version 3.

 Usage: imgur [options]

 Options:
   --auth                  Ensure authentication with an imgur.com user account
   --forget                Forget previous authentication
   --forget-auth           Forget previous authentication and authenticate with a new one
   --current               Outputs the username of the currently connected account

   --debug                 Enable more detailed error messages at standard output
   --local <filename>      Specifies local file for storing user data (default: ~/.config/ImgurQt/user)

   --upload <filename>     Upload image with given filename to user account
   --anonymous <filename>  Upload image with given filename anonymously
   --delete <delete_hash>  Delete an image with the given deletion hash

   -h, --help              Display this help message
)d"
                  << std::endl;

        // After displaying the help message, we do nothing else and quit
        return 0;

    }

    // Parse all command line options
    for(int i = 1; i < argc; ++i) {

        if(std::string(argv[i]) == "--auth")
            authAccount = true;
        else if(std::string(argv[i]) == "--forget")
            forgetAccount = true;
        else if(std::string(argv[i]) == "--forget-auth")
            forgetAndAuthAccount = true;
        else if(std::string(argv[i]) == "--upload" && argc > i+1) {
            anonymousUpload = false;
            uploadFile = QString(argv[i+1]);
        } else if(std::string(argv[i]) == "--anonymous" && argc > i+1) {
            anonymousUpload = true;
            uploadFile = QString(argv[i+1]);
        } else if(std::string(argv[i]) == "--local" && argc > i+1)
            localAccountData = QString(argv[i+1]);
        else if(std::string(argv[i]) == "--delete" && argc > i+1)
            delHash = QString(argv[i+1]);
        else if(std::string(argv[i]) == "--debug")
            debug = true;
        else if(std::string(argv[i]) == "--current")
            current = true;

    }

    // Create some space, display an empty line
    std::cout << std::endl;

    // Intialise the application instance
    QCoreApplication a(argc, argv);

    // Set up an ImgurQt instance
    ShareOnline::Imgur imgur(localAccountData, debug);

    // Some signal/slot connections
    QObject::connect(&imgur, &ShareOnline::Imgur::imgurUploadProgress, &uploadProgress);
    QObject::connect(&imgur, &ShareOnline::Imgur::imgurImageUrl, &imageUrl);
    QObject::connect(&imgur, &ShareOnline::Imgur::imgurDeleteHash, &deleteHash);
    QObject::connect(&imgur, &ShareOnline::Imgur::imgurUploadError, &uploadError);
    QObject::connect(&imgur, &ShareOnline::Imgur::abortAllRequests, qApp, &QCoreApplication::quit);
    QObject::connect(&imgur, &ShareOnline::Imgur::finished, qApp, &QCoreApplication::quit);
    QObject::connect(qApp, &QCoreApplication::aboutToQuit, &sayGoodbye);

    if(current) {

        if(imgur.authAccount() == ShareOnline::NOERROR) {

            // Output info
            std::cout << "Connected to user account of user '" << imgur.getAccountUsername().toStdString() << "'" << std::endl;

            // Quit as soon as event handling is available
            QTimer::singleShot(0,qApp, SLOT(quit()));

            // Don't go any further
            return a.exec();

        } else {

            // Output that no info is available
            std::cout << "Currently not connected to any user account..." << std::endl;

            // Quit as soon as event handling is available
            QTimer::singleShot(0,qApp, SLOT(quit()));

            // Don't go any further
            return a.exec();

        }

    }

    // Connect to an account
    if(forgetAndAuthAccount) {

        // What is going on
        std::cout << "Authenticating with an imgur.com user account..." << std::endl;

        // Obtain authorisation URL
        QString web = imgur.authorizeUrlForPin();
        std::cout << "Go to this website to obtain a PIN code: " << web.toStdString() << std::endl;

        // Ask user to enter the PIN they obtained
        std::string pin;
        std::cout << "Enter here the PIN you obtain: ";
        std::cin >> pin;

        // Handle PIN provided by user
        int ret = imgur.authorizeHandlePin(QByteArray::fromStdString(pin));

        // Check return value
        if(ret == ShareOnline::NOERROR)
            std::cout << "Successfully authenticated with user account of '" << imgur.getAccountUsername().toStdString() << "'" << std::endl;
        else
            std::cout << "An error occured! Error code: " << ret << std::endl;

        // Quit as soon as event handling is available
        QTimer::singleShot(0,qApp, SLOT(quit()));

        // Don't go any further
        return a.exec();

    }

    // Forget any authenticated account
    if(forgetAccount) {

        // What is going on
        std::cout << "Forgetting authentication to imgur.com user account... ";

        // Forget user account
        int ret = imgur.forgetAccount();

        // Check return code
        if(ret == ShareOnline::NOERROR)
            std::cout << "Done!" << std::endl;
        else
            std::cout << "ERROR! Error code: " << ret << std::endl;

        // Quit as soon as event handling is available
        QTimer::singleShot(0,qApp, SLOT(quit()));

        // Don't go any further
        return a.exec();

    }

    if(authAccount) {

        // This means there hasn't been an existing configuration file
        if(imgur.authAccount() == ShareOnline::FILENAME_ERROR) {

            // Inform the user
            std::cout << "No existing authentication found... Starting authentication process." << std::endl << std::endl;

            // Obtain authorisation URL
            QString web = imgur.authorizeUrlForPin();
            std::cout << "Go to this website to obtain a PIN code: " << web.toStdString() << std::endl;

            // Ask user to enter the PIN they obtained
            std::string pin;
            std::cout << "Enter here the PIN you obtain: ";
            std::cin >> pin;

            // Handle PIN provided by user
            int ret = imgur.authorizeHandlePin(QByteArray::fromStdString(pin));

            // Check return code
            if(ret == ShareOnline::NOERROR)
                std::cout << "Successfully authenticated with user account of '" << imgur.getAccountUsername().toStdString() << "'" << std::endl;
            else
                std::cout << "An error occured! Error code: " << ret << std::endl;

        } else
            // Nothing needs to be done
            std::cout << "Found existing authentication with user account of '" << imgur.getAccountUsername().toStdString() << "' :-)" << std::endl;

        // Quit as soon as event handling is available
        QTimer::singleShot(0,qApp, SLOT(quit()));

        // Don't go any further
        return a.exec();

    }

    // Check whether the uploadFile is a valid filename
    if(!QFileInfo(uploadFile).exists() && delHash == "") {

        // Display error message
        std::cout << "ERROR! The specified image with filename '" << uploadFile.toStdString() << "' doesn't seem to exist... Quitting now!" << std::endl;

        // Quit as soon as event handling is available
        QTimer::singleShot(0,qApp, SLOT(quit()));

        // Don't go any further
        return a.exec();

    }

    // If we have an upload file...
    if(uploadFile != "") {

        // NOT an anonymous upload, i.e., a authentication with an imgur.com user account is required
        if(!anonymousUpload) {

            // Tell the user what ImgurQt is going to do
            std::cout << "Starting upload of file '" << QFileInfo(uploadFile).fileName().toStdString() << "'" << std::endl << std::endl;

            // If no existing authentication to a user account is available
            int con = imgur.authAccount();
            if(con == ShareOnline::FILENAME_ERROR) {

                // Obtain authorisation URL
                QString web = imgur.authorizeUrlForPin();
                std::cout << "Go to this website to obtain a PIN code: " << web.toStdString() << std::endl;

                // Ask user to enter the PIN they obtained
                std::string pin;
                std::cout << "Enter here the PIN you obtain: ";
                std::cin >> pin;

                // Handle PIN provided by user
                int ret = imgur.authorizeHandlePin(QByteArray::fromStdString(pin));
                if(ret != ShareOnline::NOERROR) {
                    std::cout << "Error connecting to account! Error code: " << ret << std::endl;
                    QTimer::singleShot(0,qApp, SLOT(quit()));
                    return a.exec();
                }

            }

            // Upload image file
            if(con == ShareOnline::NOERROR || con == ShareOnline::FILENAME_ERROR) {
                int ret = imgur.upload(uploadFile);
                if(ret != ShareOnline::NOERROR)
                    std::cout << "Call to upload() failed! Error code: " << ret << std::endl;
            }

        // Anonymously upload image to imgur.com
        } else {

            // Tell the user what ImgurQt is going to do
            std::cout << "Starting anonymous upload of file '" << QFileInfo(uploadFile).fileName().toStdString() << "'" << std::endl << std::endl;

            // Upload image file
            int ret = imgur.anonymousUpload(uploadFile);
            if(ret != ShareOnline::NOERROR)
                std::cout << "Call to anonymousUpload() failed! Error code: " << ret << std::endl;

        }

    // If we don't have an upload file, then what has been passed on should be a deleteHash
    } else if(delHash != "") {

        // If no existing authentication to a user account is available
        int con = imgur.authAccount();
        if(con == ShareOnline::FILENAME_ERROR) {

            // Obtain authorisation URL
            QString web = imgur.authorizeUrlForPin();
            std::cout << "Go to this website to obtain a PIN code: " << web.toStdString() << std::endl;

            // Ask user to enter the PIN they obtained
            std::string pin;
            std::cout << "Enter here the PIN you obtain: ";
            std::cin >> pin;

            // Handle PIN provided by user
            int ret = imgur.authorizeHandlePin(QByteArray::fromStdString(pin));
            if(ret != ShareOnline::NOERROR) {
                std::cout << "Error connecting to account! Error code: " << ret << std::endl;
                QTimer::singleShot(0,qApp, SLOT(quit()));
                return a.exec();
            }

        }

        // Upload image file
        if(con == ShareOnline::NOERROR || con == ShareOnline::FILENAME_ERROR) {
            int ret = imgur.deleteImage(delHash);
            if(ret != ShareOnline::NOERROR)
                std::cout << "Deleting image failed with error code: " << ret << std::endl;
            else
                std::cout << "Successfully deleted image." << std::endl;
            QTimer::singleShot(0,qApp, SLOT(quit()));
        }
    }

    // Arrived at the end of the main() function
    return a.exec();

}

// Display upload progress
void uploadProgress(double perc) {
    int percentage = (int)(100*perc);
    std::cout << "Upload " << percentage << "% completed" << std::endl;
    if(percentage == 100) std::cout << std::endl;
}

// Display image URL (if empty, then operation was aborted)
void imageUrl(QString url) {
    if(url == "")
        std::cout << "Operation aborted..." << std::endl;
    else
        std::cout << "Image URL: " << url.toStdString() << std::endl;
}

// Display image deletion hash (if empty, then operation was aborted)
void deleteHash(QString url) {
    if(url == "")
        std::cout << "Operation aborted..." << std::endl;
    else
        std::cout << "Delete Hash: " << url.toStdString() << " (Either pass it back to ImgurQt using the '--delete' flag or go to 'http://imgur.com/delete/" << url.toStdString() << "')" << std::endl;
}

// Display an error message that has occured
void uploadError(QNetworkReply::NetworkError err) {
    std::cout << "Upload Error encountered! Network error code: " << err << std::endl;
}

// At the end, we politely say goodbye
void sayGoodbye() {
    std::cout << std::endl << "Goodbye!" << std::endl << std::endl;
}

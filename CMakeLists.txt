######################################################
# CMakeLists for ImgurQt: http://photoqt.org/imgurqt #
######################################################

CMAKE_MINIMUM_REQUIRED(VERSION 2.8)
PROJECT(imgurqt)

#############################
####  GET list of files  ####
#############################

SET(imgurqt_SOURCES src/imgur.cpp src/simplecrypt.cpp src/config.h main.cpp)
SET(imgurqt_HEADERS src/imgur.h src/simplecrypt.h src/config.h)


################################################
#### ENABLE C++11 FEATURES AND OPTIMISATION ####
################################################

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -O3 -Wall")


################################
#### FIND REQUIRED PACKAGES ####
################################

FIND_PACKAGE(Qt5 COMPONENTS Network Core REQUIRED)

######################
#### FINISHING UP ####
######################

QT5_WRAP_CPP(imgurqt_HEADERS_MOC ${imgurqt_HEADERS})

ADD_EXECUTABLE(imgurqt ${imgurqt_SOURCES} ${imgurqt_HEADERS_MOC})
QT5_USE_MODULES(imgurqt Network Core)
